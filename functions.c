#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "functions.h"

char pocetniTekst[] = "Ulaz u program.";

Question* questions;
Player* players;
int playerCount = 0;

int allocateQuestions() {
    questions = (Question*)calloc(MAX_QUESTIONS, sizeof(Question));
    if (questions == NULL) {
        perror("allocateQuestions");
        return 1;
    }
    return 0;
}

void freeQuestions() {
    free(questions);
}

int allocatePlayers() {
    players = (Player*)calloc(MAX_PLAYERS, sizeof(Player));
    if (players == NULL) {
        perror("allocatePlayers");
        return 1;
    }
    return 0;
}

void freePlayers() {
    free(players);
}

void initializeQuestions() {

strcpy(questions[0].question, "Koji je glavni grad Francuske?");
        strcpy(questions[0].correctAnswer, "London");
        strcpy(questions[0].wrongAnswer1, "Pariz");
        strcpy(questions[0].wrongAnswer2, "Berlin");
        strcpy(questions[0].wrongAnswer3, "Rim");
        questions[0].correctOption = 'B';
    
        strcpy(questions[1].question, "Koji je najveci ocean na svijetu?");
        strcpy(questions[1].correctAnswer, "Pacifik");
        strcpy(questions[1].wrongAnswer1, "Atlantski");
        strcpy(questions[1].wrongAnswer2, "Indijski");
        strcpy(questions[1].wrongAnswer3, "Juzni");
        questions[1].correctOption = 'A';
    
        // Add more questions as needed
        strcpy(questions[2].question, "Koliko kontinenata postoji na Zemlji?");
        strcpy(questions[2].correctAnswer, "6");
        strcpy(questions[2].wrongAnswer1, "5");
        strcpy(questions[2].wrongAnswer2, "7");
        strcpy(questions[2].wrongAnswer3, "8");
        questions[2].correctOption = 'C';
    
        strcpy(questions[3].question, "Koja je najmanja država na svijetu?");
        strcpy(questions[3].correctAnswer, "Vatikan");
        strcpy(questions[3].wrongAnswer1, "Monako");
        strcpy(questions[3].wrongAnswer2, "San Marino");
        strcpy(questions[3].wrongAnswer3, "Lihtenštajn");
        questions[3].correctOption = 'A';
    
        strcpy(questions[4].question, "Koji je najduži riječni sistem na svijetu?");
        strcpy(questions[4].correctAnswer, "Misisipi");
        strcpy(questions[4].wrongAnswer1, "Amazon");
        strcpy(questions[4].wrongAnswer2, "Jangce");
        strcpy(questions[4].wrongAnswer3, "Nil");
        questions[4].correctOption = 'D';
    
        strcpy(questions[5].question, "Koji je najveći planet u našem Sunčevom sustavu?");
        strcpy(questions[5].correctAnswer, "Mars");
        strcpy(questions[5].wrongAnswer1, "Saturn");
        strcpy(questions[5].wrongAnswer2, "Zemlja");
        strcpy(questions[5].wrongAnswer3, "Jupiter");
        questions[5].correctOption = 'D';
    
        strcpy(questions[6].question, "Koji je najviše planinski vrh na svijetu?");
        strcpy(questions[6].correctAnswer, "Mount Everest");
        strcpy(questions[6].wrongAnswer1, "K2");
        strcpy(questions[6].wrongAnswer2, "Kangchenjunga");
        strcpy(questions[6].wrongAnswer3, "Lhotse");
        questions[6].correctOption = 'A';
    
        strcpy(questions[7].question, "Koja je najduža rijeka u Europi?");
        strcpy(questions[7].correctAnswer, "Dunav");
        strcpy(questions[7].wrongAnswer1, "Volga");
        strcpy(questions[7].wrongAnswer2, "Rajna");
        strcpy(questions[7].wrongAnswer3, "Seine");
        questions[7].correctOption = 'B';
    
        strcpy(questions[8].question, "Koji je najveći otok na svijetu?");
        strcpy(questions[8].correctAnswer, "Grenland");
        strcpy(questions[8].wrongAnswer1, "Novi Zeland");
        strcpy(questions[8].wrongAnswer2, "Borneo");
        strcpy(questions[8].wrongAnswer3, "Madagaskar");
        questions[8].correctOption = 'A';
    
        strcpy(questions[9].question, "Koji je najviši vodopad na svijetu?");
        strcpy(questions[9].correctAnswer, "Angelov vodopad");
        strcpy(questions[9].wrongAnswer1, "Niagara");
        strcpy(questions[9].wrongAnswer2, "Victoria Falls");
        strcpy(questions[9].wrongAnswer3, "Tugela Falls");
        questions[9].correctOption = 'A';
    
}

void askQuestion(int questionNumber) {
    printf("\nPitanje %d: %s\n", questionNumber + 1, questions[questionNumber].question);
    printf("A. %s\n", questions[questionNumber].correctAnswer);
    printf("B. %s\n", questions[questionNumber].wrongAnswer1);
    printf("C. %s\n", questions[questionNumber].wrongAnswer2);
    printf("D. %s\n", questions[questionNumber].wrongAnswer3);
}

char getUserAnswer() {
    char answer;
    printf("Unesite odgovor (A/B/C/D): ");
    scanf(" %c", &answer);
    return answer;
}

void func(int* score) {
    initializeQuestions();
    int currentQuestion = 0;
    int winnings = 0;

    while (currentQuestion < MAX_QUESTIONS) {
        askQuestion(currentQuestion);
        char userAnswer = getUserAnswer();

        if (userAnswer == questions[currentQuestion].correctOption) {
            printf("Tocan odgovor!\n");
            winnings += 1000;
        } else {
            printf("Krivi odgovor!\n");
            printf("Osvojili ste %d kuna!\n", winnings);
            break;
        }

        currentQuestion++;
        if(currentQuestion == 10){
        	printf("Pobjedio si.");
        	break;
        }
    }

    *score = winnings;
}

void addPlayer(const char* playerName) {
    FILE* file = fopen("players.txt", "a");
    if (file == NULL) {
        perror("addPlayer");
        return;
    }

    fprintf(file, "%s 0\n", playerName); // Initialize with a score of 0
    fclose(file);
    printf("Igrac uspjesno dodan.\n");
}

void deletePlayer(const char* playerName) {
    FILE* tempfile = fopen("temp.txt", "w");
    FILE* file = fopen("players.txt", "r");
    if (file == NULL || tempfile == NULL) {
        perror("deletePlayer");
        return;
    }

    char line[100];
    int found = 0;

    while (fgets(line, sizeof(line), file)) {
        line[strcspn(line, "\n")] = '\0';
        char existingPlayer[100];
        int score;
        sscanf(line, "%s %d", existingPlayer, &score);

        if (strcmp(existingPlayer, playerName) == 0) {
            found = 1;
        } else {
            fprintf(tempfile, "%s %d\n", existingPlayer, score);
        }
    }
    fclose(file);
    fclose(tempfile);

    if (!found) {
        printf("Igrac nije pronaden.\n");
        remove("temp.txt");
    } else {
        remove("players.txt");
        rename("temp.txt", "players.txt");
        printf("Igrac uspjesno izbrisan.\n");
    }
}

int findPlayer(const char* playerName, int* score) {
    FILE* file = fopen("players.txt", "r");
    if (file == NULL) {
        perror("findPlayer");
        return 0;
    }

    char line[100];
    while (fgets(line, sizeof(line), file)) {
        line[strcspn(line, "\n")] = '\0';
        char existingPlayer[100];
        int existingScore;
        sscanf(line, "%s %d", existingPlayer, &existingScore);

        if (strcmp(existingPlayer, playerName) == 0) {
            *score = existingScore;
            fclose(file);
            return 1;
        }
    }

    fclose(file);
    return 0;
}

void updatePlayerScore(const char* playerName, int newScore) {
    FILE* tempfile = fopen("temp.txt", "w");
    FILE* file = fopen("players.txt", "r");
    if (tempfile == NULL || file == NULL) {
        perror("updatePlayerScore");
        return;
    }

    char buffer[100];
    while (fgets(buffer, sizeof(buffer), file)) {
        buffer[strcspn(buffer, "\n")] = '\0';
        char existingPlayer[100];
        int existingScore;
        sscanf(buffer, "%s %d", existingPlayer, &existingScore);

        if (strcmp(existingPlayer, playerName) == 0) {
            fprintf(tempfile, "%s %d\n", playerName, newScore);
        } else {
            fprintf(tempfile, "%s %d\n", existingPlayer, existingScore);
        }
    }

    fclose(file);
    fclose(tempfile);
    remove("players.txt");
    rename("temp.txt", "players.txt");
}

void readPlayers() {
    playerCount = 0; // Reset player count before reading
    FILE* file = fopen("players.txt", "r");
    if (file == NULL) {
        perror("readPlayers");
        return;
    }

    while (fscanf(file, "%s %d", players[playerCount].playerName, &players[playerCount].score) != EOF) {
        playerCount++;
    }

    fclose(file);
}

void bubbleSortPlayers() {
    for (int i = 0; i < playerCount - 1; i++) {
        for (int j = 0; j < playerCount - i - 1; j++) {
            if (players[j].score < players[j + 1].score) { // Sort in descending order
                Player temp = players[j];
                players[j] = players[j + 1];
                players[j + 1] = temp;
            }
        }
    }
}

void printSortedPlayers() {
    printf("Igraci sortirani po rezultatu:\n");
    for (int i = 0; i < playerCount; i++) {
        printf("%s: %d\n", players[i].playerName, players[i].score);
    }
}



void quitProgram(){
	printf("Zelite li sigurno izaci(da/ne)");
	char response[10];
    scanf("%9s", response);
    if (strcmp(response, "da") == 0) {
    	freeQuestions();
    	freePlayers();
    	exit(0);
    } else if(strcmp(response, "ne") == 0) {
    	return;
	}

	quitProgram();
	
}
