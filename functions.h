#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#define MAX_QUESTIONS 100
#define MAX_QUESTION_LENGTH 100
#define MAX_ANSWER_LENGTH 50
#define MAX_PLAYERS 100

typedef struct {
    char question[MAX_QUESTION_LENGTH];
    char correctAnswer[MAX_ANSWER_LENGTH];
    char wrongAnswer1[MAX_ANSWER_LENGTH];
    char wrongAnswer2[MAX_ANSWER_LENGTH];
    char wrongAnswer3[MAX_ANSWER_LENGTH];
    char correctOption;
} Question;

typedef struct {
    char playerName[100];
    int score;
} Player;

int allocateQuestions();
void freeQuestions();
int allocatePlayers();
void freePlayers();
void initializeQuestions();
void askQuestion(int questionNumber);
char getUserAnswer();
void func(int* score);
void addPlayer(const char* playerName);
void deletePlayer(const char* playerName);
int findPlayer(const char* playerName, int* score);
void updatePlayerScore(const char* playerName, int newScore);
void readPlayers();
void bubbleSortPlayers();
void printSortedPlayers();
void quitProgram();

extern Question* questions;
extern Player* players;
extern int playerCount;

extern char pocetniTekst[];


enum Opcije{
	DodajIgraca = 1,
	IzbrisiIgraca,
	Igraj,
	Izlaz,
	IspisIgraca	
};

#endif
