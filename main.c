#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "functions.h"

void clearInputBuffer() {
	int c;
	while ((c = getchar()) != '\n' && c != EOF);
}



int main() {
	printf("%s", pocetniTekst);
    static int choice;
    char playerName[100];
    int playerScore = 0;

    if (allocateQuestions() != 0 || allocatePlayers() != 0) {
        printf("Memory allocation failed. Exiting...\n");
        return 1;
    }

    while (1) {
        printf("\n===== MILIJUNAS =====\n");
        printf("1. Dodaj igraca\n");
        printf("2. Izbrisi igraca\n");
        printf("3. Igraj\n");
        printf("4. Izlaz\n");
        printf("5. Ispis igraca\n");
        printf("Unesi svoj izbor: ");
        do {
            scanf("%d", &choice);
            printf("Izabrao si: %c", getchar());
            printf("%d\n", choice);
            if (choice < 1 || choice >  5) {
                printf("Neispravan unos. Ponovno unesite izbor: ");
                clearInputBuffer();
            }
        } while (choice < 1 || choice > 5);

        switch (choice) {
            case DodajIgraca:
                printf("Unesi ime igraca: ");
                scanf("%s", playerName);
                addPlayer(playerName);
                break;
            case IzbrisiIgraca:
                printf("Unesi ime igraca za obrisati: ");
                scanf("%s", playerName);
                deletePlayer(playerName);
                break;
            case Igraj:
                printf("Unesi ime igraca: ");
                scanf("%s", playerName);
                if (!findPlayer(playerName, &playerScore)) {
                    printf("Igrac ne postoji. Zelis li ga kreirati? (da/ne): ");
                    char response[10];
                    scanf("%9s", response);
                    if (strcmp(response, "da") == 0) {
                        addPlayer(playerName);
                    } else {
                        break;
                    }
                }
                func(&playerScore);
                updatePlayerScore(playerName, playerScore);
                break;
            case Izlaz:
                printf("Izlazenje iz programa.\n");
                quitProgram();
            case IspisIgraca:
                readPlayers();
                bubbleSortPlayers();
                printSortedPlayers();
                break;
            default:
                printf("Neispravan unos. Pokusaj ponovno.\n");
        }
        clearInputBuffer();
    }

    return 0;
}
